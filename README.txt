Image Style Deliver 404
-----------------------
eosrei 4/8/2014

Display the standard themeable Drupal 404 response instead of a bare 500 when
an image style derivative cannot be generated.

If you have a website with many images and image styles your site will be
indexed by Google Image Search and other image search engines. If images or
image styles change your vistor will see a bare "Error generating image." on
the resulting 500 status page. Terrible UX right there. This module corrects
the situation by returning the standard themeable Drupal 404 page.

Frustrating that all this code needs to be duplicated to change a few lines.

